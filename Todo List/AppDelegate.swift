//
//  AppDelegate.swift
//  Todo List
//
//  Created by Elisha Narida on 05/07/2017.
//  Copyright © 2017 Imergex. All rights reserved.
//

import UIKit
import Firebase
import Alamofire

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        FirebaseApp.configure() //to call firebase when the app starts
        AnonymousSignIn()
        
        return true
    }
}

// function for AnonymousSignIn
func AnonymousSignIn() {
    
    if Connectivity.isConnectedToInternet() { //if internet connection
        
        Auth.auth().signInAnonymously { (user, error) in
            
            let uid = user!.uid
            print("User ID: \(uid)")
        }
        
    }else{
        print("no internet connectivity")
    }
}

class Connectivity { //Module on Alamofire
    class func isConnectedToInternet() ->Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}


