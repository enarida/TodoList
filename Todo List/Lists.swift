//
//  Lists.swift
//  Todo List
//
//  Created by Elisha Narida on 05/07/2017.
//  Copyright © 2017 Imergex. All rights reserved.
//

import Foundation
import UIKit
import Firebase

class Lists: UITableViewController {
    
    let cellString = "Cell" //Cell identifier
    var tField: UITextField! //TextField for the alertView
    var lists: [String] = [] //variable of lists
    
    var ref: DatabaseReference? //Database Reference of firebase
    
    var handle: DatabaseHandle? //Database Handler for the listener
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        ref = Database.database().reference() // connecting to the databas references
        
        handle = ref?.child("Lists").observe(.childAdded, with: { (snapshot) in //observe func when an child child value added on my database
            
            if let itemsOnFirebase = snapshot.value as? String { //converts the database value to string
                
                self.lists.append(itemsOnFirebase) // adds the value to my lists arrays
                self.tableView.reloadData() //reloads data
                
            }
        })
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1 //returns the number of sections on row
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lists.count //returns the number of rows in sections
    }
    
    //creating the format of the cell
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellString, for: indexPath)  //connecting the cell to tableview
        cell.textLabel?.text! = lists[indexPath.row]  // CellTitle = String(This is the title)
        return cell
    }
    
    
    @IBAction func AddItemTapped(_ sender: Any) {
        addItem()
        
    }
    
    //Function to have an alertview
    func addItem() {
        
        let alert = UIAlertController(title: "Enter a list", message: "Please enter a new list below.", preferredStyle: UIAlertControllerStyle.alert)
        alert.addTextField(configurationHandler: configurationTextField) //adds textfield on alertview
        
        let doneAction = UIAlertAction(title: "Add", style: UIAlertActionStyle.default, handler:{ (UIAlertAction) in //adds done action on alertview
            
            let addedMessage = self.tField.text!
            self.ref?.child("Lists").childByAutoId().setValue(addedMessage) // to send value of the "Added Message" to my firebase database
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil) //adds cancel action on alertview
        
        alert.addAction(doneAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func configurationTextField(textField: UITextField!){ //config on textfield
        if (textField) != nil {
            tField = textField
        }else {
            print("no input added")
        }
    }
}

